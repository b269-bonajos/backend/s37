// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// This allows us to use all the routes defined in userRoutes.js
const userRoute = require("./routes/userRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
// localhost:4000/users
app.use("/users", userRoute);

// Database Connection
mongoose.connect("mongodb+srv://sepienkhel19:admin123@zuitt-bootcamp.bnuv3za.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log(`Now connected to cloud database`));





// Server listening
// Will used the defined port number for the application whenever environment variable is available to used port 4000 if non is defined
// this syntax will allow flexibility when using the application locally or as hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));
