// s37 Activity
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	Name : {
		type : String,
		required : [true, "First Name and Last Name is required"]
	},
	description : ("Serves to state the rationale for the course and give an overview of key content covered, skills and knowledge to be learned, and how it will benefit the student.")
	price : 85.000
	isActive : {
		type : Boolean,
		default : false
	},
	createdOn : {
			type : String,
			default : new Date()}
	enrollments : 
		{
			userId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
		}
});

module.exports = mongoose.model("User", userSchema);